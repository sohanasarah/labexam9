-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 24, 2016 at 05:03 PM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `labexam9`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) NOT NULL,
  `first_name` varchar(300) NOT NULL,
  `last_name` varchar(300) NOT NULL,
  `email` varchar(300) NOT NULL,
  `phone_no` varchar(300) NOT NULL,
  `password` varchar(300) NOT NULL,
  `gender` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `phone_no`, `password`, `gender`) VALUES
(1, '12', '23', 'sarah@example.com', '1234', '1234', ''),
(2, 'ss', 'ss', 'ss@gmail.com', '1234', '3691308f2a4c2f6983f2880d32e29c84', ''),
(3, 'sarah', 'sohana', '123@gmail.com', '01988329821', '202cb962ac59075b964b07152d234b70', ''),
(4, 'afiyaA', 'ayman', 'aa@gmail.com', '01234567', '', ''),
(5, 'Sarahh', 'Sohanaa', 'asarahsohana@gmail.com', '012345678', '1234', 'female'),
(6, 'afiyaaa', 'ayman', 'aa@yahoo.com', '1234', '81dc9bdb52d04dc20036dbd8313ed055', ''),
(7, '', '', 'aa@yahoo.com', '', '4124bc0a9335c27f086f24ba207a4912', ''),
(8, '', '', 'aa@gmail.com', '', '4124bc0a9335c27f086f24ba207a4912', ''),
(9, 'sarah', 'sohana', '124@gmail.com', '1234', 'c8ffe9a587b126f152ed3d89a146b445', 'female'),
(13, '', '', 'sarah@gmail.com', '', '674f3c2c1a8a6f90461e8a66fb5550ba', ''),
(14, '', '', 'sarah@gmail.com', '', '81dc9bdb52d04dc20036dbd8313ed055', ''),
(15, '12', '34', 'abc@example.com', '0123', '4ed9407630eb1000c0f6b63842defa7d', 'male');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
