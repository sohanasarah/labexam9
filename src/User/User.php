<?php
namespace App\ User;
use App\Message\Message;
use App\Model\Database as DB;
use App\Utility\Utility;
session_start();
//include_once('../../vendor/autoload.php');

class User extends DB{
    public $table="users";
    public $firstname="";
    public $lastname="";
    public $email="";
    public $password="";
    public $phone_no="";
    public $gender="";
    public $id;

    public function __construct()
    {
        parent::__construct();
    }

    public function prepare($data){
        if(array_key_exists('first_name',$data)){
            $this->firstname=$data['first_name'];
        }
        if(array_key_exists('last_name',$data)){
            $this->lastname=$data['last_name'];
        }

        if(array_key_exists('id',$data)){
            $this->id=$data['id'];
        }
        if(array_key_exists('phone_no',$data)){
            $this->phone_no=$data['phone_no'];
        }
        if(array_key_exists('gender',$data)){
            $this->gender=$data['gender'];
        }
        if(array_key_exists('email',$data)){
            $this->email=$data['email'];
        }
        if(array_key_exists('password',$data)){
            $this->password=md5($data['password']);
        }

        return $this;
    }

    public function store(){
        $query="INSERT INTO `labexam9`.`users` (`first_name`, `last_name`, `email`, `phone_no`, `password`, `gender`) VALUES ( '".$this->firstname."', '".$this->lastname."', '".$this->email."', '".$this->phone_no."', '".$this->password."', '".$this->gender."')";
        $result=mysqli_query($this->conn,$query);
        if($result){
            Message::message("<div class='alert alert-success'>Stored!</div>");
            Utility::redirect("../../index.php");

        }
        else{
            Utility::redirect("../../index.php");

        }
    }
    public function update(){

        $query="UPDATE `labexam9`.`users` SET `first_name` = '".$this->firstname."', `last_name` = '".$this->lastname."', `email` = '".$this->email."', `phone_no` = '".$this->phone_no."', `password` = '".$this->password."', `gender` ='".$this->gender."' WHERE `users`.`id` = ".$this->id;
        //echo $query;
        $result = mysqli_query($this->conn,$query);
        if($result)
        {
            echo "Updated";
            //Message::message("<div class='alert alert-success'>Updated!</div>");
            //Utility::redirect('../../views/welcome.php');
        }
        else
        {
            echo "Error!";
        }
    }
    public function view($data)
    {
        $query = "SELECT * FROM `users` WHERE `email`='" . $data . "'";
        $result = mysqli_query($this->conn, $query);
        $row = mysqli_fetch_assoc($result);
        return $row;
    }




}