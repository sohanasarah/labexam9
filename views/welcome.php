<?php
//echo "WELCOME";
//session_start();
include_once("../vendor/autoload.php");
use App\User\Auth;
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;

$auth=new Auth();
$status= $auth->logged_in();
if($status==FALSE){
    Message::message("<div class='alert alert-danger'>You have to log in first</div>");
    return Utility::redirect("../index.php");

}
$singleUser=$auth->view($_SESSION['user_email']);
//$user= new User();
//var_dump($singleUser);
//die();
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Landing Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h1 align="center">Welcome!</h1>
    <ul class="nav nav-tabs">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="edit.php">Edit data</a></li>
        <li><a href="Authentication/logout.php">Logout</a></li>
    </ul>
    <div class="tab-content">
        <div id="home" class="tab-pane fade in active">
            <h3>Firstname</h3>
            <p><?php echo $singleUser['first_name']?></p>
        </div>
        <div id="menu1" >
            <h3>Lastname</h3>
            <p><?php echo $singleUser['last_name']?></p>
        </div>
        <div id="menu2" >
            <h3>Email</h3>
            <p><?php echo $singleUser['email']?></p>
        </div>
        <div id="menu2" >
            <h3>Phone no</h3>
            <p><?php echo $singleUser['phone_no']?></p>
        </div>
        <div id="menu2" >
            <h3>Gender</h3>
            <p><?php echo $singleUser['gender']?></p>
        </div>
        <div id="menu2" >
            <h3>Password</h3>
            <p><?php echo $singleUser['password']?></p>
        </div>
    </div>
    <br>


</div>

</body>
</html>

