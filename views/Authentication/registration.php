<?php
session_start();
include_once('../../vendor/autoload.php');
use App\User\Auth;
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;

$auth=new Auth();
$status= $auth->prepare($_POST)->is_exit();
if($status)
{
    Message::setMessage("<div class='alert alert-danger'><strong>Already Exits!</strong></div>");
    Utility::redirect("../../index.php");

}

else
{
    $user= new User();
    $user->prepare($_POST)->store();
}