<?php
session_start();
include_once('../../vendor/autoload.php');
use App\User\Auth;
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;

$auth=new Auth();
$status= $auth->prepare($_POST)->is_registered();
if($status)
{
    $_SESSION['user_email']=$_POST['email'];
    Utility::redirect("../welcome.php");

}

else
{
    $user= new User();
    $user->prepare($_POST)->store();
}