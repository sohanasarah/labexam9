<?php
session_start();
include_once('../../vendor/autoload.php');
use App\User\Auth;
use App\User\User;
use App\Message\Message;
use App\Utility\Utility;

$auth=new Auth();
$status=$auth->logged_out();

if($status)
{
    Message::setMessage("<div class='alert alert-success'><strong>You have successfully logged out!</strong></div>");
    Utility::redirect("../../index.php");
}