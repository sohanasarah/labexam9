<?php
include_once('../vendor/autoload.php');
use App\User\User;
$user= new User();
$user->prepare($_POST);
$singleItem=$user->view($_SESSION['user_email']);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Book</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Edit Information</h2>
    <form role="form" method="post" action="update.php">
        <div class="form-group">
            <label><b>Edit Information:</b></label>
            <br>
            <input type="hidden" name="id" id="user" value="<?php echo $singleItem['id'] ?>">
            <label>FirstName:</label>
            <input type="text" name="first_name" class="form-control"  value="<?php echo $singleItem['first_name']?>">
            <label>Last Name:</label>
            <input type="text" name="last_name" class="form-control"  value="<?php echo $singleItem['last_name']?>">
            <label>Email:</label>
            <input type="text" name="email" class="form-control"  value="<?php echo $singleItem['email']?>">
            <label>Phone No:</label>
            <input type="text" name="phone_no" class="form-control"  value="<?php echo $singleItem['phone_no']?>">
            <label>Password:</label>
            <input type="password" name="password" class="form-control"  value="<?php echo $singleItem['password']?>">
            <label>Gender:</label>
            <div class="form-group">
                <div class="radio">
                    <label><input type="radio" name="gender" value="female" <?php if($singleItem['gender']=="female"){
                            echo "checked";
                        }?>>Female</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="gender" value="male" <?php if($singleItem['gender']=="male"){
                            echo "checked";
                        }?>>Male</label>
                </div>
                <div class="radio">
                    <label><input type="radio" name="gender" value="other" <?php if($singleItem['gender']=="other"){
                            echo "checked";
                        }?>>Other</label>
                </div>

            </div>
        </div>

        <button type="submit" class="btn btn-default">Update</button>
    </form>
</div>

</body>
</html>